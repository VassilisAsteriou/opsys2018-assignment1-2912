#!/bin/bash

tryCloneGitUrl() {
    url=$1

    if echo "$url" | grep ^https:// &>/dev/null
    then
        if git clone "$url" &>/dev/null
        then
            echo "$url: Cloning OK"
        else
            echo "$url: Cloning FAILED"
        fi
    fi
}

examineFile() {
    filename="$1"

    while IFS='' read -r line || [[ -n "$line" ]]
    do
        [[ "$line" = \#* ]] && continue
        cd "assignments"
        tryCloneGitUrl "$line"
        cd ..
        break
    done < "$filename"
}

printTotalResults() {
    dirname="assignments/$1"

    nodirs=$( for i in $(find "$dirname"/* ); do file $i; done | grep -c 'directory$' )
    notxts=$( find "$dirname"/* | grep -c '.txt$' )
    total=$( find "$dirname"/* | wc -l )
    noother=$(( $total - $nodirs - $notxts ))

    echo "$1:"
    echo "Number of directories: $nodirs"
    echo "Number of txt files: $notxts"
    echo "Number of other files: $noother"
}

makeModelDir() {
    mkdir -p '.model-directory'
    cd '.model-directory'

      # Directories
      mkdir -p more

      # Files
      touch dataA.txt
      touch more/dataB.txt
      touch more/dataC.txt

    cd ..
}

checkStructure() {
    dirname="assignments/$1"

    diff <(cd '.model-directory'; find | grep -v .git) \
         <(cd "$dirname"; find | grep -v .git) &>/dev/null \
         && status='OK' \
         || status='NOT OK'

    echo "Directory structure is $status"
}

mkdir -p ".tmp-dir"
rm -rf "assignments" &>/dev/null
mkdir -p "assignments"

tar -xvf "$1" -C ".tmp-dir" &>/dev/null

for file in `find ".tmp-dir" | grep '.txt\$'`
do
    examineFile "$file"
done

makeModelDir

for directory in $(ls "assignments")
do
    printTotalResults "$directory"
    checkStructure "$directory"
done

rm -r '.tmp-dir'
rm -r '.model-directory'
